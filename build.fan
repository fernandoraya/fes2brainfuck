using build

class Build : BuildPod
{
  new make()
  {
    podName = "brainfuck"    
    summary = "Brainfuck language interpreter programmed in Fantom" 
    version = Version("0.2.0")
    
    meta = [      
      "license.name" : "GPL-3",
      "proj.name"    : "brainfuck",
      "repo.public"  : "true",
      "repo.tags"    : "interpreter, brainfuck",
      "vcs.name"     : "Git",
      "vcs.uri"      : "https://bitbucket.org/fernandoraya/fes2brainfuck" 
    ]
    
    depends = [
      "sys 1.0+", 
      "util 1.0+"
    ]
    
    srcDirs = [
      `fan/`,
      `test/`
    ]    
    
    resDirs = [
      `res/`,
      `doc/`
    ]

    docApi = true
    docSrc = true
  }
}
