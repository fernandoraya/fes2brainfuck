# fes2brainfuck

Brainfuck interpreter in Fantom programming language.

## Usage

fan brainfuck <brainfuck-program>

## Examples

In folder 'res' there are some brainfuck programs. I'm sorry I don't
remember the source of all of them.

There are more examples in `http://esoteric.sange.fi/brainfuck/bf-source/prog/`.

## Optimizations

- [X] Remove comments
- [X] Precomputed jumps
- [X] Group increment-data instructions
- [X] Group decrement-data instructions
- [X] Group increment-pointer
- [X] Group decrement-pointer
