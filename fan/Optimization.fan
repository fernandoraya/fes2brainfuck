mixin Optimizable 
{
  abstract Void optimize()
  abstract Instruction? next
}

mixin NonOptimizable : Optimizable
{
  override Void optimize() {}
}

mixin GroupOptimizable : Optimizable
{
  abstract protected Int amount

  protected Bool nextIsLikeMe()
  {
    next != null && next.typeof == this.typeof
  }

  override Void optimize()
  {
    while (nextIsLikeMe)
    {
      amount++
      next = next.next
    }
  }
}
