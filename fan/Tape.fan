**
** Array of bytes initialized to zero used to read/write data.
**
class Tape
{
  const static Log log := Tape#.pod.log
  const static Int defTapeSize := 3000

  new make(Int size := defTapeSize)
  {
    this.data = Int[,].fill(0, size)
  }

  Int item()
  {
    data[dp]
  }

  Bool itemIsZero()
  {
    this.item == 0
  }

  @Operator
  Int get(Int index)
  {
    data[index]
  }

  @Operator
  This set(Int index, Int value)
  {
    data[index] = value
    return this
  }

  This put(Int value)
  {
    this[dp] = value
  }
  
  This increment(Int amount := 1)
  {
    put(this.item + amount)
  }

  This decrement(Int amount := 1)
  {
    put(this.item - amount)
  }

  This forth(Int amount := 1)
  {
    if (dp >= size) throw MemoryOverflowErr()
    dp = dp + amount
    return this
  }

  This back(Int amount := 1)
  {
    if (dp <= 0) throw MemoryUnderflowErr()
    dp = dp - amount
    return this
  }

  Int size()
  {
    data.size
  }

  override Int hash()
  {
    data.hash
  }

  override Bool equals(Obj? that)
  {
    if (that isnot Tape)
      return false

    other := (Tape) that

    if (other.size != this.size)
      return false

    if (this.dp != other.dp)
      return false

    return data.all |Int v, Int i -> Bool|
    {
      return v == other[i]
    }
  }

  Void writeOn(OutStream out)
  {
    out.print("Tape@${dp}:")
    data.each { out.print(it) }
  }

  override Str toStr()
  {
    "tape@${dp}:${data[dp]}"
  }

  Int dp
  {
    get { return &dp }
  }
  
  private Int[] data
}
