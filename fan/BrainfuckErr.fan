**
** Error produced in the interpretation of an instruction.
**
const class BrainfuckErr : Err
{
  new make(Str msg := "", Err? cause := null) 
  : super(msg, cause)
  {}
}

**
** Error produced when a pointer reads out of the tape 
** superior limit.
**
const class MemoryOverflowErr : BrainfuckErr
{
  new make(Err? cause := null)
  : super("Memory pointer overflow", cause)
  {}
}

**
** Error produced when a pointer reads out of the tape
** inferior limit.
**
const class MemoryUnderflowErr : BrainfuckErr
{
  new make(Err? cause := null)
  : super("Memory pointer underflow", cause)
  {}
}

const class AddressErr : BrainfuckErr
{
  new make(Err? cause := null)
  : super("Program out of bounds", cause)
  {}
}


const class MismatchedJump : BrainfuckErr
{
  new make(Err? cause := null)
  : super("Mismatched Jump instruction", cause)
  {}
}
