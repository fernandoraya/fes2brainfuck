**
** Application exit codes.
**
** When the application exits returns the ordinal number
** of the enum.
**
enum class ExitCode
{
  **
  ** Program exited with success
  **
  success,

  **
  ** Program exited with error because program arguments
  **
  errArgs,

  **
  ** Program exited with error because a runtime error 
  ** happened executing the program.
  **
  errRuntime,

  **
  ** Program exited with an unexpected error
  **
  errUnexpected
}
