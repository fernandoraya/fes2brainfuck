mixin Jump : TapeInstruction, ProgramInstruction
{}

class JumpForward : Jump
{
  override protected Program program
  override protected Tape tape
  override Instruction? next  
  protected JumpBackward? jump

  new make(Program program, Tape tape)
  {
    this.program = program
    this.tape = tape
  }

  override Void optimize()
  {    
    level := 1
    Instruction? cur := this

    while (level > 0)
    {
      cur = cur.next

      if (cur == null)
        throw MismatchedJump()

      switch (cur.typeof)
      {
        case JumpForward#:
          level++
        case JumpBackward#:
	        level--
        default:
      }
    }

    jump = (JumpBackward) cur
    jump.jump = this    
  }

  override Void execute()
  {
    if (tape.itemIsZero) program.jump(jump)
  }

  override Str toStr() { "[" }
}

class JumpBackward : Jump, NonOptimizable
{ 
  override protected Program program
  override protected Tape tape
  override Instruction? next
  protected JumpForward? jump
  

  new make(Program program, Tape tape)
  {
    this.program = program
    this.tape = tape
  }

  override Void execute()
  {
    if (!tape.itemIsZero) program.jump(jump)
  }

  override Str toStr() { "]" }
}

