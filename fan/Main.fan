using util

class Main : AbstractMain
{
  @Arg { help = "Brainfuck program"}
  File? file
  
  @Opt { help = "Show debug information" }
  Bool debug

  override Int run()
  {
    if (!file.exists)
    {
      log.err("`${file}`: file does not exists")
      return ExitCode.errArgs.ordinal
    }
    
    if (debug)
      log.level = LogLevel.debug
    
    try
    {
      bf := Brainfuck(file.in)
      bf.optimize
      bf.run
      echo
      return ExitCode.success.ordinal
    }
    catch (AddressErr e)
    {
      log.err(e.msg)
      return ExitCode.errRuntime.ordinal
    }
    catch (BrainfuckErr e)
    {
      log.err(e.msg)
      return ExitCode.errRuntime.ordinal 
    }
    catch (Err e)
    {
      log.err("Unexpected error", e)
      return ExitCode.errUnexpected.ordinal 
    }
  }
}
