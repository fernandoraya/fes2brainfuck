mixin Instruction : Optimizable
{
  abstract Void execute()
}

mixin TapeInstruction : Instruction
{
  abstract protected Tape tape()
}

mixin ProgramInstruction : Instruction
{
  abstract protected Program program()
}
