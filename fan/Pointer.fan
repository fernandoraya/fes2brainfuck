abstract class PointerInstruction : TapeInstruction, GroupOptimizable
{
  override protected Tape tape
  override Int amount := 1
  override Instruction? next
  
  new make(Tape tape) { this.tape = tape }
}

class IncrementPointer : PointerInstruction
{
  new make(Tape tape) : super.make(tape) {}
  override Void execute() { tape.forth(amount) }
  override Str toStr() { "${amount}>" }
}

class DecrementPointer : PointerInstruction
{
  new make(Tape tape) : super.make(tape) {}  
  override Void execute() { tape.back(amount) }
  override Str toStr() { "<${amount}" }
}