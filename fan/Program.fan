class Program
{
  Instruction? first
  Instruction? last
  Instruction? current

  new make() 
  {}

  Bool isEmpty()
  {
    first == null
  }

  @Operator
  This add(Instruction instruction)
  {
    if (isEmpty)
    {
      first = last = current = instruction
    }
    else
    {
      last.next = instruction
      last = instruction
    }

    return this
  }

  @Operator
  Instruction get(Int index)
  {
    if (index < 0)
      throw IndexErr("Index negative ${index}")
    
    i := 0
    c := first
    while (c != null && i != index)
    {
      c = c.next
      i++
    }

    if (c == null)
      throw IndexErr("Index out of bounds ${index}")
    return c
  }

  Void jump(Instruction? instruction)
  {
    current = instruction
  }

  Bool isFinished()
  {
    current == null
  }

  Void execute()
  {
    current.execute
  }

  Void forth()
  {
    current = current?.next
  }

  Void each(|Instruction instruction, Int index| c)
  {
    i := 0
    p := first
    while (p != null)
    {
      c(p,i)
      p = p.next
      i++
    }
  }

  Void writeOn(OutStream out)
  {
    cur := first
    while (cur != null)
    {
      out.print(cur.toStr)
      cur = cur.next
    }
  }

  Int size()
  {
    n := 0
    cur := first
    while (cur != null)
    {
      cur = cur.next
      n++
    }
    return n
  }

  Void optimize()
  {
    cur := first
    while (cur != null)
    {
      cur.optimize
      cur = cur.next
    }
  }
}
