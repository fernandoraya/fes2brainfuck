**
** Interpreter for brainfuck instructions
** 
class Brainfuck
{
  const static Log log := Brainfuck#.pod.log

  **
  ** Array to write
  ** 
  Tape tape := Tape(3000)

  **
  ** Instructions
  **
  Program program := Program()

  ** 
  new make(InStream in)
  {
    c := in.readChar
    
    while(c != null)
    {
      switch (c)
      {
        case '+':
	  add(IncrementData(tape))
	case '-':
	  add(DecrementData(tape))
	case '>':
	  add(IncrementPointer(tape))
	case '<':
	  add(DecrementPointer(tape))	
	case '[':
	  add(JumpForward(program, tape))
	case ']':
	  add(JumpBackward(program, tape))
	case ',':
	  add(Input(tape))
	case '.':
	  add(Output(tape))
	default:
      }       
      c = in.readChar
    } 
  }

  @Operator
  This add(Instruction instruction)
  {
    this.program.add(instruction)
    return this
  }
  
  Void optimize()
  {
    program.optimize
  }

  Void run()
  {
    while (!program.isFinished)
    {
      if (log.isDebug) log.debug(this.toStr)
      program.execute
      program.forth
    }
  }

  Void writeOn(OutStream out)
  {
    out.printLine("Brainfuck dump")
    program.writeOn(out)
    out.printLine
    out.print("Tape")
    tape.writeOn(out)
    out.printLine
  }
}
