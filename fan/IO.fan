class Input : TapeInstruction, NonOptimizable
{
  override protected Tape tape
  override Instruction? next

  new make(Tape tape) { this.tape = tape }  
  
  override Void execute()
  {
    valid := false

    while(!valid)
    {
      try
      {
        n := Env.cur.in.readLine.toInt
        if (n < 0 || n > 255)
          echo("Invalid input: must be a number between 0 and 255")
        else
        {
          tape.put(n)
          valid = true
        }
      }
      catch (Err e)
      {
        echo("Invalid input: not a number")
      }
    }
  }

  override Str toStr() { "," }
}

class Output : TapeInstruction, NonOptimizable
{
  override protected Tape tape
  override Instruction? next

  new make(Tape tape) { this.tape = tape }
  
  override Void execute()
  {
    Env.cur.out.print(tape.item.toChar).flush
  }

  override Str toStr() { "." }
}