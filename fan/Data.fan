abstract class DataInstruction : TapeInstruction, GroupOptimizable
{
  override Instruction? next
  override protected Int amount := 1
  override protected Tape tape
  
  new make(Tape tape) { this.tape = tape }
}

class IncrementData : DataInstruction
{
  new make(Tape tape) : super.make(tape) {}
  override Void execute() { tape.increment(amount) }
  override Str toStr() { "${amount}+" }
}

class DecrementData : DataInstruction
{
  new make(Tape tape) : super.make(tape) {}
  override Void execute() { tape.decrement(amount) }
  override Str toStr() { "${amount}-" }
}