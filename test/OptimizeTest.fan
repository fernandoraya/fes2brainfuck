class OptimizeTest : Test
{
  Void testGroup()
  {
    bf := Brainfuck("++++-->><<".in)
    bf.optimize
    optimized := bf.program

    verifyEq(optimized.size, 4)
    verifyType(optimized[0], IncrementData#)
    verifyType(optimized[1], DecrementData#)
    verifyType(optimized[2], IncrementPointer#)
    verifyType(optimized[3], DecrementPointer#)
  }
}
