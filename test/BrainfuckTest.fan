class BrainfuckTest : Test
{
  Void testProgram1()
  {
    bf := Brainfuck("+>++>+++".in)
    bf.optimize
    bf.run

    verifyEq(bf.tape[0], 1)
    verifyEq(bf.tape[1], 2)
    verifyEq(bf.tape[2], 3)
    verifyEq(bf.tape[3], 0)
  }

  Void testProgram2()
  {
    verifyErr(MemoryUnderflowErr#)
    {
      bf := Brainfuck("+<<".in)
      bf.optimize
      bf.run
    }
  }

  Void testProgram3()
  {
    bf := Brainfuck("[ [foo bar then]]+".in)
    bf.optimize
    bf.run
    verifyEq(bf.program[4].typeof, IncrementData#)
    verifyEq(bf.tape[0], 1)
  }
}
