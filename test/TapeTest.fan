class TapeTest : Test
{ 
  Void testConstructor()
  {
    t1 := Tape(3)
    verifyEq(t1.dp, 0)
    verifyEq(t1.size, 3)
  }

  Void testSet()
  {
    t1 := Tape(3)    
    t1[0] = 1

    verifyEq(t1.size, 3)
    verifyEq(t1[0], 1)
    verifyEq(t1[1], 0)
    verifyEq(t1[2], 0)
  }

  Void testIncrement()
  {
    t1 := Tape(3)        
    t1.increment
    verifyEq(t1[0], 1)

    t1.increment(3)
    verifyEq(t1[0], 4)
  }

  Void testDecrement()
  {
    t1 := Tape(3)
    t1.decrement
    verifyEq(t1[0], -1)

    t1.decrement(3)
    verifyEq(t1[0], -4)
  }

  Void testForth()
  {
    t1 := Tape(3)
    t1.forth
    verifyEq(t1.dp, 1)

    verifyErr(MemoryOverflowErr#)
    {
      t1.forth.forth.forth
    }
  }

  Void testBack()
  {
    t1 := Tape(3)
    t1.forth.back
    verifyEq(t1.dp, 0)

    verifyErr(MemoryUnderflowErr#)
    {
      t1.back
    }
  }

  Void testEqual()
  {
    t1 := Tape(3)
    t2 := Tape(3)
    verifyEq(t1, t1)
    verifyNotEq(t1, t2.forth)
    verifyNotEq(t1, t2.back.increment)
  }
}
